package me.flyray.bsin.gateway.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import me.flyray.bsin.gateway.domain.BsinChoreographyServiceDo;
import me.flyray.bsin.gateway.mapper.ChoreographyServiceMapper;
import me.flyray.bsin.gateway.service.BsinChoreographyServiceService;

@Service
public class ChoreographyServiceServiceImpl implements BsinChoreographyServiceService {


    @Autowired
    private ChoreographyServiceMapper choreographyServiceMapper;

    @Override
    public BsinChoreographyServiceDo getChoreographyServiceByServiceAndMethod(Map<String, Object> requestMap) {
        String serviceName = (String) requestMap.get("serviceName");
        String methodName = (String) requestMap.get("methodName");
        BsinChoreographyServiceDo choreographyServiceDo = choreographyServiceMapper.selectByServiceNameAndMethodName(serviceName, methodName);
        return choreographyServiceDo;
    }
}
