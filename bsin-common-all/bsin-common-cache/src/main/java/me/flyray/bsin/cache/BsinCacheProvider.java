package me.flyray.bsin.cache;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

@Service
public class BsinCacheProvider {

    @Autowired
    private StringRedisTemplate redisTemplate;

    public <T> void set(String key, T value) {
        String json = JSON.toJSON(value).toString();
        //将json对象转换为字符串
        redisTemplate.opsForValue().set(key, json);
    }

    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public void setEx(String key, String value,Long expireTime) {
        redisTemplate.opsForValue().set(key, value, expireTime, TimeUnit.SECONDS);
    }

    public void sAdd(String key, String value) {
        redisTemplate.opsForSet().add(key, value);
    }

    public void sAdd(String key, String... value) {
        redisTemplate.opsForSet().add(key,value);
    }

    /**
     * 获取无序集合
     * @param key1
     */
    public Set<String> getMembers(String key1) {
        return redisTemplate.opsForSet().members(key1);
    }

    /**
     * 求两个无序集合的交集
     * @param key1
     * @param key2
     */
    public void setIntersect(String key1, String key2) {
        redisTemplate.opsForSet().intersect(key1,key2);
    }

    /**
     * 求两个无序集合的交集存在 key3集合中
     * @param key1
     * @param key2
     * @param key3
     */
    public void setIntersectAndStore(String key1, String key2, String key3) {
        redisTemplate.opsForSet().intersectAndStore(key1,key2,key3);
    }

    /**
     * 无序集合的并集
     * @param key1
     * @param key2
     */
    public void setUnion(String key1, String key2) {
        redisTemplate.opsForSet().union(key1,key2);
    }

    /**
     * 求两个结合的并集存在 key3中
     * @param key1
     * @param key2
     * @param key3
     */
    public void setUnionAndStore(String key1, String key2, String key3) {
        redisTemplate.opsForSet().unionAndStore(key1,key2,key3);
    }

    /**
     * 求两个无序集合的差集存入key3集合中
     * @param key1
     * @param key2
     * @param key3
     */
    public void setDifferenceAndStore(String key1, String key2, String key3) {
        redisTemplate.opsForSet().differenceAndStore(key1,key2,key3);
    }

    /**
     * 求两个无序集合的差集
     * @param key1
     * @param key2
     */
    public void setDifference(String key1, String key2) {
        redisTemplate.opsForSet().difference(key1,key2);
    }

    public boolean set(String key, String value, long validTime) {
        boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
            @Override
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
                connection.set(serializer.serialize(key), serializer.serialize(value));
                connection.expire(serializer.serialize(key), validTime);
                return true;
            }
        });
        return result;
    }

    public <T> T get(String key, Class<T> clazz) {
        JSONObject jso= JSON.parseObject(get(key));
        return JSONObject.toJavaObject(jso, clazz);
    }

    public <T> List<T> getList(String key, Class<T> clazz) {
        return JSONObject.parseArray(get(key), clazz);
    }

    public String get(String key) {
        return redisTemplate.opsForValue().get(key);
        /*String result = template.execute(new RedisCallback<String>() {
            @Override
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer<String> serializer = template.getStringSerializer();
                byte[] value = connection.get(serializer.serialize(key));
                return serializer.deserialize(value);
            }
        });
        return result;*/
    }

    public boolean del(String key) {
        return redisTemplate.delete(key);
    }

}
